import React from "react";
import "./App.css";
import Home from "./Home";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMap: false,
      showMatrikkelInput: false
    };
  }

  render() {
    return (
      <div className="App">
        <div id="leaflet-container">
          <Home />
        </div>
      </div>
    );
  }
}

export default Main;
